
<?php
    include "library/header-css.php";
?>
    <header>
        <div class="header-main">
            <div class="container-fluid d-flex justify-content-between align-items-center flex-wrap">
                <div class="moblie-bar d-block d-md-block d-lg-none">
                    <button class="icon-mobile toggle hc-nav-trigger hc-nav-1">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="logo">
                    <a href="index.php">
                        <img src="assets/images/logo.png" alt="">
                    </a>
                </div>

                <div class="main-menu">
                    <ul>
                        <li>
                            <a href="">Packages</a>
                        </li>
                        <li class = "has-child">
                            <a href="">Service</a>
                            <ul>
                                <li><a href="">Content 1</a></li>
                                <li><a href="">Content 2</a></li>
                                <li><a href="">Content 3</a></li>
                                <li><a href="">Content 4</a></li>
                                <li><a href="">Content 5</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Portfolio</a>
                        </li>
                        <li>
                            <a href="">Blog</a>
                        </li>
                        <li>
                            <a href="">About</a>
                        </li>
                        <li>
                            <div class="languages">
                                <a href="">VN</a>
                                |
                                <a href="">EN</a>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="search-mb d-block d-md-block d-lg-none">
                    <button id="btn-search-hd">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </header>
