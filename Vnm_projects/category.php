<?php
    include "header.php";
?>

<main>
    <section class="breadcumb-main">
        <div class="container-fluid d-flex flex-wrap justify-content-between align-items-center">
            <div class="main-title others">
                <h1 class="title">
                    Product list
                </h1>
            </div>

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Library</li>
            </ol>
        </div>
    </section>

    <section class="main-category">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-sm-12 col-md-3 col-lg-3">

                </div>

                <div class="col-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="product-box">
                                <div class="box-images">
                                    <a href="">
                                        <img src="assets/images/ca1.png" alt="">
                                    </a>
                                </div>

                                <div class="box-text text-center">
                                    <div class="title-product">
                                        <h3>
                                            <a href="">Wooden chest</a>
                                        </h3>
                                    </div>

                                    <div class="desc">
                                        <span>
                                            Nullam laoreet arcu eu massa euismod, quis dictum ..
                                        </span>
                                    </div>

                                    <div class="price-product">
                                        <span class="price">
                                            <del>
                                                <span>$276.22</span>
                                            </del> 

                                            <ins>
                                                <span>$256.22</span>
                                            </ins>
                                        </span>                          
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="product-box">
                                <div class="box-images">
                                    <a href="">
                                        <img src="assets/images/ca1.png" alt="">
                                    </a>
                                </div>

                                <div class="box-text text-center">
                                    <div class="title-product">
                                        <h3>
                                            <a href="">Wooden chest</a>
                                        </h3>
                                    </div>

                                    <div class="desc">
                                        <span>
                                            Nullam laoreet arcu eu massa euismod, quis dictum ..
                                        </span>
                                    </div>

                                    <div class="price-product">
                                        <span class="price">
                                            <del>
                                                <span>$276.22</span>
                                            </del> 

                                            <ins>
                                                <span>$256.22</span>
                                            </ins>
                                        </span>                          
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="product-box">
                                <div class="box-images">
                                    <a href="">
                                        <img src="assets/images/ca1.png" alt="">
                                    </a>
                                </div>

                                <div class="box-text text-center">
                                    <div class="title-product">
                                        <h3>
                                            <a href="">Wooden chest</a>
                                        </h3>
                                    </div>

                                    <div class="desc">
                                        <span>
                                            Nullam laoreet arcu eu massa euismod, quis dictum ..
                                        </span>
                                    </div>

                                    <div class="price-product">
                                        <span class="price">
                                            <del>
                                                <span>$276.22</span>
                                            </del> 

                                            <ins>
                                                <span>$256.22</span>
                                            </ins>
                                        </span>                          
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="product-box">
                                <div class="box-images">
                                    <a href="">
                                        <img src="assets/images/ca1.png" alt="">
                                    </a>
                                </div>

                                <div class="box-text text-center">
                                    <div class="title-product">
                                        <h3>
                                            <a href="">Wooden chest</a>
                                        </h3>
                                    </div>

                                    <div class="desc">
                                        <span>
                                            Nullam laoreet arcu eu massa euismod, quis dictum ..
                                        </span>
                                    </div>

                                    <div class="price-product">
                                        <span class="price">
                                            <del>
                                                <span>$276.22</span>
                                            </del> 

                                            <ins>
                                                <span>$256.22</span>
                                            </ins>
                                        </span>                          
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="product-box">
                                <div class="box-images">
                                    <a href="">
                                        <img src="assets/images/ca1.png" alt="">
                                    </a>
                                </div>

                                <div class="box-text text-center">
                                    <div class="title-product">
                                        <h3>
                                            <a href="">Wooden chest</a>
                                        </h3>
                                    </div>

                                    <div class="desc">
                                        <span>
                                            Nullam laoreet arcu eu massa euismod, quis dictum ..
                                        </span>
                                    </div>

                                    <div class="price-product">
                                        <span class="price">
                                            <del>
                                                <span>$276.22</span>
                                            </del> 

                                            <ins>
                                                <span>$256.22</span>
                                            </ins>
                                        </span>                          
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="product-box">
                                <div class="box-images">
                                    <a href="">
                                        <img src="assets/images/ca1.png" alt="">
                                    </a>
                                </div>

                                <div class="box-text text-center">
                                    <div class="title-product">
                                        <h3>
                                            <a href="">Wooden chest</a>
                                        </h3>
                                    </div>

                                    <div class="desc">
                                        <span>
                                            Nullam laoreet arcu eu massa euismod, quis dictum ..
                                        </span>
                                    </div>

                                    <div class="price-product">
                                        <span class="price">
                                            <del>
                                                <span>$276.22</span>
                                            </del> 

                                            <ins>
                                                <span>$256.22</span>
                                            </ins>
                                        </span>                          
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="product-box">
                                <div class="box-images">
                                    <a href="">
                                        <img src="assets/images/ca1.png" alt="">
                                    </a>
                                </div>

                                <div class="box-text text-center">
                                    <div class="title-product">
                                        <h3>
                                            <a href="">Wooden chest</a>
                                        </h3>
                                    </div>

                                    <div class="desc">
                                        <span>
                                            Nullam laoreet arcu eu massa euismod, quis dictum ..
                                        </span>
                                    </div>

                                    <div class="price-product">
                                        <span class="price">
                                            <del>
                                                <span>$276.22</span>
                                            </del> 

                                            <ins>
                                                <span>$256.22</span>
                                            </ins>
                                        </span>                          
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="product-box">
                                <div class="box-images">
                                    <a href="">
                                        <img src="assets/images/ca1.png" alt="">
                                    </a>
                                </div>

                                <div class="box-text text-center">
                                    <div class="title-product">
                                        <h3>
                                            <a href="">Wooden chest</a>
                                        </h3>
                                    </div>

                                    <div class="desc">
                                        <span>
                                            Nullam laoreet arcu eu massa euismod, quis dictum ..
                                        </span>
                                    </div>

                                    <div class="price-product">
                                        <span class="price">
                                            <del>
                                                <span>$276.22</span>
                                            </del> 

                                            <ins>
                                                <span>$256.22</span>
                                            </ins>
                                        </span>                          
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>


<?php
    include "footer.php";
?>