    <footer id = "footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="title-ft">
                        <h3>contact</h3>
                    </div>

                    <div class="the_content content_ft">
                        <p>
                            <strong>Email: </strong><span>Contact@vnmhome.com</span>
                        </p>
                        <p>
                            <strong>Tel: </strong><span>(+84) 098 137 2580</span>
                        </p>
                    </div>

                    <div class="follow-ic">
                        <ul>
                            <li>
                                <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="title-ft">
                        <h3>location</h3>
                    </div>

                    <div class="the_content content_ft">
                        <p>
                            <strong>Studio: </strong><span>No. 26/2, Alley 12 Dang Thai Mai Quang An, Tay Ho, Hanoi</span>
                        </p>  
                        <p>
                            <strong>Office: </strong><span>109 Tran Hung Dao, Hoan Kiem, Hanoi</span>
                        </p>                     
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="title-ft">
                        <h3>policy terms</h3>
                    </div>

                    <div class="the_content content_ft">
                        <ul class="navi-ft">
                            <li>
                                <a href="">Company introduction</a>
                            </li>
                            <li>
                                <a href="">Recruitment</a>
                            </li>
                            <li>
                                <a href="">Rules</a>
                            </li>
                            <li>
                                <a href="">Privacy Policy</a>
                            </li>
                        </ul>                   
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="cd-top">Back To Top</a>
    </footer>
    <div class="wrapper cf">
        <nav id="main-nav">
            <ul>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-18 current_page_item menu-item-27">
                    <a href="index.php" aria-current="page">Trang chủ</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26">
                    <a href="">Packages</a>
                </li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-taxonomies_services menu-item menu-item-30">
                    <a href="">Service</a>
                    <ul>
                        <li><a href="">Content 1</a></li>
                        <li><a href="">Content 2</a></li>
                        <li><a href="">Content 3</a></li>
                        <li><a href="">Content 4</a></li>
                        <li><a href="">Content 5</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item menu-item-23"><a href="">Portfolio</a></li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-taxonomies_project menu-item-36"><a href="">Blog</a></li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-28">
                    <div class="languages">
                        <a href="">VN</a>
                        |
                        <a href="">EN</a>
                    </div>
                </li>
            </ul>
        </nav>
    </div>

    <div class="box-search_header">
        <label for="">Search</label>

        <form action="">
            <input type="text" placeholder="Search">
            <button type="submit" class="btn-hd-search">
                <i class="fa fa-search" aria-hidden="true"></i>
            </button>
        </form>
    </div>
    <div class="over-lay-mb"></div>
<?php
    include "library/footer-js.php";
?>