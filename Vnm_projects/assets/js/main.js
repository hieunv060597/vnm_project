(function ($) {
    window.onload = function () {
        $(document).ready(function () {		
            back_to_top();
            moblie_bar();
            resize_banner();
            slider_banner();
            slider_packages();
            slider_porfolio();
            resize_to_before();
            slider_before();
            before_after();
            slider_review();
            slider_text_pablo();
            open_search_hd();
        });               
    };
})(jQuery);

function back_to_top() {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration
		);
	});
}

function stuck_header(){
    var top_bar = document.querySelector('.top-bar');
    var header_main = document.querySelector('.header-main')
    var header_bottom = document.querySelector('.header-bottom');
    var check = true;

    if(top_bar == null && header_main == null && header_bottom == null){
        return 0;
    }

    else{
        window.addEventListener('scroll', function(){
            if(window.pageYOffset > 260){
                if(check == true){
                    top_bar.classList.add('off-hd');
                    header_main.classList.add('off-hd');
                    header_bottom.classList.add('stuck');
                    check = false;
                }
            }
            else{
                if(check == false){
                    top_bar.classList.remove('off-hd');
                    header_main.classList.remove('off-hd');
                    header_bottom.classList.remove('stuck');
                    check = true;
                }
            }
        })
    }
}

function moblie_bar() {
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
        maxWidth: false,
        customToggle: $toggle,
        // navTitle: 'All Categories',
        levelTitles: true,
        pushContent: '#container'
    };

    // add new items to original nav
    $main_nav.find('li.add').children('a').on('click', function() {
        var $this = $(this);
        var $li = $this.parent();
        var items = eval('(' + $this.attr('data-add') + ')');

        $li.before('<li class="new"><a>' + items[0] + '</a></li>');

        items.shift();

        if (!items.length) {
            $li.remove();
        } else {
            $this.attr('data-add', JSON.stringify(items));
        }

        Nav.update(true);
    });

    // call our plugin
    var Nav = $main_nav.hcOffcanvasNav(defaultData);

    // demo settings update

    const update = (settings) => {
        if (Nav.isOpen()) {
            Nav.on('close.once', function() {
                Nav.update(settings);
                Nav.open();
            });

            Nav.close();
        } else {
            Nav.update(settings);
        }
    };

    $('.actions').find('a').on('click', function(e) {
        e.preventDefault();

        var $this = $(this).addClass('active');
        var $siblings = $this.parent().siblings().children('a').removeClass('active');
        var settings = eval('(' + $this.data('demo') + ')');

        update(settings);
    });

    $('.actions').find('input').on('change', function() {
        var $this = $(this);
        var settings = eval('(' + $this.data('demo') + ')');

        if ($this.is(':checked')) {
            update(settings);
        } else {
            var removeData = {};
            $.each(settings, function(index, value) {
                removeData[index] = false;
            });

            update(removeData);
        }
    });
}

function open_search_hd(){
	var btn_op_search = document.querySelectorAll('#btn-search-hd');
	var over_lay_search = document.querySelector('.over-lay-mb');
    var box_search_hd = document.querySelector('.box-search_header');
    if(btn_op_search == null && over_lay_search == null && box_search_hd  == null){
        return 0;
    }
    else{
        for(var i = 0; i<btn_op_search.length; i++){
            btn_op_search[i].onclick = function(){
                over_lay_search.classList.add('overlay-open');
                box_search_hd.classList.add('active_show');
            }
        }
        over_lay_search.onclick = function(){
            over_lay_search.classList.remove('overlay-open');
            box_search_hd.classList.remove('active_show');
        }
    }
	
}

function resize_banner(){
    var banner_img = $('.banner-box .banner-images');
    var banner = $('.banner-box .banner');
    if(banner_img == null){
        return 0;
    }
    else{
        var width = $(window).width();
        var height = banner_img.height();
        height = width / 2.2453;
        banner_img.height(height);
    }
}

function slider_banner(){
    $('.banner .owl-carousel').owlCarousel({
        loop:true,
        margin: 0,
        autoplay:true,
        nav:false,
        dots: true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            576:{
                items:1,
            },
            768:{
                items:1,
            },
            1024:{
                items:1,
            },

            1400:{
                items:1,
            }
        }
   })
}

function slider_packages(){
    $('.our-packet .owl-carousel').owlCarousel({
        loop:true,
        margin: 30,
        autoplay:true,
        nav:false,
        dots: true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            576:{
                items:1,
            },
            768:{
                items:2,
            },
            1024:{
                items:3,
            },

            1400:{
                items:4,
            }
        }
   })
}

function slider_porfolio(){
    $('.porfolio .owl-carousel').owlCarousel({
        loop:true,
        margin: 90,
        autoplay:true,
        nav:true,
        navText: [
            "<i class = 'fa fa-arrow-left'></i>",
            "<i class = 'fa fa-arrow-right'></i>",
          ],
        dots: false,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                margin: 30,
            },
            576:{
                items:1,
                margin: 30,
            },
            768:{
                items:2,
                margin: 30,
            },
            1024:{
                items:3,
                margin: 30,
            },

            1400:{
                items:3,
            }
        }
   })
}

function before_after(){
    $(".twentytwenty-container").twentytwenty();
}

function resize_to_before(){
    var before_img = $('.before-after .twentytwenty-container img');

    if(before_img == null){
        return 0;
    }
    else{
        var width = $('.before-after .container').width();
        var height = before_img.height();
        height = width / 2.3;
        before_img.height(height);
    }
}

function slider_before(){
    $('.before-after .owl-carousel').owlCarousel({
        loop:true,
        margin: 0,
        autoplay:true,
        nav:true,
        navText: [
            "<i class = 'fa fa-arrow-left'></i>",
            "<i class = 'fa fa-arrow-right'></i>",
          ],
        dots: false,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        mouseDrag:false,
        touchDrag: false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            576:{
                items:1,
            },
            768:{
                items:1,
            },
            1024:{
                items:1,
            },

            1400:{
                items:1,
            }
        }
   })
} 

function slider_review(){
    $('.review-customer .owl-carousel').owlCarousel({
        loop:true,
        margin: 90,
        autoplay:true,
        nav:false,
        dots: true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                margin: 30,
            },
            576:{
                items:1,
                margin: 30,
            },
            768:{
                items:2,
                margin: 30,
            },
            1024:{
                items:3,
                margin: 30,
            },

            1400:{
                items:3,
            }
        }
   })
}

function slider_text_pablo(){
    $('.pablo-design .owl-carousel').owlCarousel({
        loop:true,
        margin: 10,
        autoplay:true,
        nav:false,
        dots: true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            576:{
                items:1,
            },
            768:{
                items:1,
            },
            1024:{
                items:1,
            },

            1400:{
                items:1,
            }
        }
   })
}