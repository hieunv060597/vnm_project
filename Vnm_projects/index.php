<?php
    include "header.php";
?>

<main class = "home-page">
    <section class="banner">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="banner-box">
                    <div class="banner-images">
                        <img src="assets/images/bn1.jpg" alt="" class = "w-100">
                    </div>

                    <div class="banner-text">
                        <h2 class="banner-title bg-title">
                            We do not only build living space
                        </h2>

                        <h3 class="banner-title sm-title">
                            We build living style
                        </h3>

                        <a href="" class="banner-btn">
                            view all packages
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="item">
                <div class="banner-box">
                    <div class="banner-images">
                        <img src="assets/images/bn1.jpg" alt="" class = "w-100">
                    </div>

                    <div class="banner-text">
                        <h2 class="banner-title bg-title">
                            We do not only build living space
                        </h2>

                        <h3 class="banner-title sm-title">
                            We build living style
                        </h3>

                        <a href="" class="banner-btn">
                            view all packages
                        </a>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="banner-box">
                    <div class="banner-images">
                        <img src="assets/images/bn1.jpg" alt="" class = "w-100">
                    </div>

                    <div class="banner-text">
                        <h2 class="banner-title bg-title">
                            We do not only build living space
                        </h2>

                        <h3 class="banner-title sm-title">
                            We build living style
                        </h3>

                        <a href="" class="banner-btn">
                            view all packages
                        </a>
                    </div>
                </div>
            </div>  
        </div> 
    </section>

    <section class="how-it-work">
        <div class="main-title text-center">
            <h2 class="title">
                How it Works
            </h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="icon-box">
                        <div class="images">
                            <img src="assets/images/ic1.png" alt="">
                            <span class="text-ct">1</span>
                        </div>

                        <div class="text">
                            <h3 class="title">
                                Choose Package
                            </h3>

                            <span class="desc">
                                Select your best fit from our interior packages
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="icon-box">
                        <div class="images">
                            <img src="assets/images/ic2.png" alt="">
                            <span class="text-ct">2</span>
                        </div>

                        <div class="text">
                            <h3 class="title">
                                Consult & Contract
                            </h3>

                            <span class="desc">
                                Let us build the perfect plan for your space
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="icon-box">
                        <div class="images">
                            <img src="assets/images/ic3.png" alt="">
                            <span class="text-ct">3</span>
                        </div>

                        <div class="text">
                            <h3 class="title">
                                Construction
                            </h3>

                            <span class="desc">
                                Our construction team will bring your dream home to live
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="icon-box">
                        <div class="images">
                            <img src="assets/images/ic4.png" alt="">
                            <span class="text-ct">4</span>
                        </div>

                        <div class="text">
                            <h3 class="title">
                                Warranty
                            </h3>

                            <span class="desc">
                                We make sure you feel happy and secure in your new home
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our-packet">
        <div class="container-fluid">
            <div class="main-title text-center">
                <h2 class="title">
                    Our Packages
                </h2>
            </div>

            <div class="the_content custom-content-home container text-center">
                <p>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</p>
            </div>

            <div class="owl-carousel owl-theme">
                <div class="item">
                    <div class="blog-box box-text-inside">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/pk1.png" alt="" class = "w-100">
                            </div>

                            <div class="box-text">
                                <h3>Seoul</h3>
                                <span>Seoul Package is designed to give you a well-rounded Seoul cityence. </span>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="item">
                    <div class="blog-box box-text-inside">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/pk1.png" alt="" alt="" class = "w-100">
                            </div>

                            <div class="box-text">
                                <h3>Seoul</h3>
                                <span>Seoul Package is designed to give you a well-rounded Seoul cityence. </span>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="item">
                    <div class="blog-box box-text-inside">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/pk1.png" alt="" alt="" class = "w-100">
                            </div>

                            <div class="box-text">
                                <h3>Seoul</h3>
                                <span>Seoul Package is designed to give you a well-rounded Seoul cityence. </span>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="item">
                    <div class="blog-box box-text-inside">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/pk1.png" alt="" alt="" class = "w-100">
                            </div>

                            <div class="box-text">
                                <h3>Seoul</h3>
                                <span>Seoul Package is designed to give you a well-rounded Seoul cityence. </span>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="item">
                    <div class="blog-box box-text-inside">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/pk1.png" alt="" alt="" class = "w-100">
                            </div>

                            <div class="box-text">
                                <h3>Seoul</h3>
                                <span>Seoul Package is designed to give you a well-rounded Seoul cityence. </span>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="item">
                    <div class="blog-box box-text-inside">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/pk1.png" alt="" alt="" class = "w-100">
                            </div>

                            <div class="box-text">
                                <h3>Seoul</h3>
                                <span>Seoul Package is designed to give you a well-rounded Seoul cityence. </span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our-product">
        <div class="container-fluid">
            <div class="main-title text-center">
                <h2 class="title">
                    Our Product
                </h2>
            </div>

            <div class="the_content custom-content-home container text-center">
                <p>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</p>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="pr-images">
                    <a href="">
                        <img src="assets/images/p1.png" alt="" class = "w-100">
                    </a>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="row">
                    <div class="col-6">
                        <div class="pr-images">
                            <a href="">
                                <img src="assets/images/p2.png" alt="" class = "w-100">
                            </a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="pr-images">
                            <a href="">
                                <img src="assets/images/p3.png" alt="" class = "w-100">
                            </a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="pr-images">
                            <a href="">
                                <img src="assets/images/p4.png" alt="" class = "w-100">
                            </a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="pr-images">
                            <a href="">
                                <img src="assets/images/p5.png" alt="" class = "w-100">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="see-more-home">
                    <a href="">
                        See more
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="porfolio">
        <div class="container">
            <div class="main-title text-center">
                <h2 class="title">
                    Porfolio
                </h2>
            </div>

            <div class="the_content custom-content-home container text-center">
                <p>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</p>
            </div>

            <div class="owl-carousel owl-theme">
                <div class="item">
                    <div class="blog-box normal-blog">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/po1.png" alt="" class = "w-100">
                            </div>
                            
                            <div class="box-text">
                                <div class="desc">
                                    <span>V&M Home is a platform that makes interior design easier than ever for everyone from an</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="item">
                    <div class="blog-box normal-blog">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/po1.png" alt="" class = "w-100">
                            </div>
                            
                            <div class="box-text">
                                <div class="desc">
                                    <span>V&M Home is a platform that makes interior design easier than ever for everyone from an</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="item">
                    <div class="blog-box normal-blog">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/po1.png" alt="" class = "w-100">
                            </div>
                            
                            <div class="box-text">
                                <div class="desc">
                                    <span>V&M Home is a platform that makes interior design easier than ever for everyone from an</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="item">
                    <div class="blog-box normal-blog">
                        <a href="">
                            <div class="box-images">
                                <img src="assets/images/po1.png" alt="" class = "w-100">
                            </div>
                            
                            <div class="box-text">
                                <div class="desc">
                                    <span>V&M Home is a platform that makes interior design easier than ever for everyone from an</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="see-more-home">
                <a href="">
                    See more
                </a>
            </div>
        </div>
    </section>

    <section class="before-after">
        <div class="container">
            <div class="main-title text-center">
                <h2 class="title">
                    Before / After
                </h2>
            </div>

            <div class="owl-carousel owl-theme">
                <div class="item">
                    <div class = "twentytwenty-container">
                        <img src="assets/images/g1.jpg" alt="Antes" class= "w-100">
                        <img src="assets/images/g2.jpg" alt="Antes" class= "w-100">
                    </div>
                </div>

                <div class="item">
                    <div class = "twentytwenty-container">
                        <img src="assets/images/g1.jpg" alt="Antes" class= "w-100">
                        <img src="assets/images/g2.jpg" alt="Antes" class= "w-100">
                    </div>
                </div>


                <div class="item">
                    <div class = "twentytwenty-container">
                        <img src="assets/images/g1.jpg" alt="Antes" class= "w-100">
                        <img src="assets/images/g2.jpg" alt="Antes" class= "w-100">
                    </div>
                </div>


                <div class="item">
                    <div class = "twentytwenty-container">
                        <img src="assets/images/g1.jpg" alt="Antes" class= "w-100">
                        <img src="assets/images/g2.jpg" alt="Antes" class= "w-100">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="review-customer">
        <div class="container">
            <div class="main-title text-center">
                <h2 class="title">
                    Customers review
                </h2>
            </div>

            <div class="the_content custom-content-home container text-center">
                <p>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</p>
            </div>

            <div class="owl-carousel owl-theme">
                <div class="item">
                    <div class="review-box">
                        <h3 class="title">
                            Great responsiveness and final product
                        </h3>

                        <div class="rating-star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>

                        <div class="desc">
                            <span>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</span>
                        </div>

                        <div class="infor-customer">
                            <h4>Tom Black</h4>
                            <h4>JYSK GROUP</h4>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="review-box">
                        <h3 class="title">
                            Great responsiveness and final product
                        </h3>

                        <div class="rating-star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>

                        <div class="desc">
                            <span>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</span>
                        </div>

                        <div class="infor-customer">
                            <h4>Tom Black</h4>
                            <h4>JYSK GROUP</h4>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="review-box">
                        <h3 class="title">
                            Great responsiveness and final product
                        </h3>

                        <div class="rating-star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>

                        <div class="desc">
                            <span>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</span>
                        </div>

                        <div class="infor-customer">
                            <h4>Tom Black</h4>
                            <h4>JYSK GROUP</h4>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="review-box">
                        <h3 class="title">
                            Great responsiveness and final product
                        </h3>

                        <div class="rating-star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>

                        <div class="desc">
                            <span>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</span>
                        </div>

                        <div class="infor-customer">
                            <h4>Tom Black</h4>
                            <h4>JYSK GROUP</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pablo-design">
        <div class="row no-gutters">
            <div class="col-12 col-sm-12 col-md-12 col-lg-5">
                <div class="img-box">
                    <img src="assets/images/po2.png" alt="" class="w-100">
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-7">
                <div class="conent-box">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="text">
                                <h3 class="title">
                                    We’ve been long fans of the modern, yet inviting lighting that Pablo Designs has to offer.”
                                </h3>

                                <div class="the_content">
                                    <p>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="text">
                                <h3 class="title">
                                    We’ve been long fans of the modern, yet inviting lighting that Pablo Designs has to offer.”
                                </h3>

                                <div class="the_content">
                                    <p>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="text">
                                <h3 class="title">
                                    We’ve been long fans of the modern, yet inviting lighting that Pablo Designs has to offer.”
                                </h3>

                                <div class="the_content">
                                    <p>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.Our name is inspired from the term “Victorian maisonette” from London</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="work-with-us">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-5">
                    <div class="content-box">
                        <h2 class="title">
                            Why customer should work with us
                        </h2>

                        <h3 class="desc">
                            V&M Home is a platform that makes interior design easier than ever for everyone
                        </h3>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-7 align-self-center">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="content-normal">
                                <div class="ic">
                                    <img src="assets/images/ic6.png" alt="">
                                </div>

                                <div class="text">
                                    <h4>Minimal Design</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="content-normal">
                                <div class="ic">
                                    <img src="assets/images/ic7.png" alt="">
                                </div>

                                <div class="text">
                                    <h4>24/7 online support</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="content-normal">
                                <div class="ic">
                                    <img src="assets/images/ic5.png" alt="">
                                </div>

                                <div class="text">
                                    <h4>Absolute safely</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="partner">
        <div class="row no-gutters">
            <div class="col-12 col-sm-12 col-md-12 col-lg-5">
                <div class="img-box">
                    <img src="assets/images/g4.png" alt="">
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-7">
                <div class="content-box">
                    <div class="main-title text-left">
                        <h2 class="title">
                            Partners
                        </h2>
                    </div>

                    <div class="the_content">
                        <p>V&M Home is a platform that makes interior design easier than ever for everyone from anywhere. We are an interior & furniture brand of Mongtasia - a design company with branches in the U.S, Korea, China and Vietnam.</p>
                        <ul class="row no-gutters">
                            <li class="col-12 col-md-6">
                                <a href="">Partners</a>
                            </li>
                            <li class="col-12 col-md-6">
                                <a href="">Partners</a>
                            </li>
                            <li class="col-12 col-md-6">
                                <a href="">Partners</a>
                            </li>
                            <li class="col-12 col-md-6">
                                <a href="">Partners</a>
                            </li>
                            <li class="col-12 col-md-6">
                                <a href="">Partners</a>
                            </li>

                            <li class="col-12 col-md-6">
                                <a href="">Partners</a>
                            </li>

                            <li class="col-12 col-md-6">
                                <a href="">Partners</a>
                            </li>

                            <li class="col-12 col-md-6">
                                <a href="">Partners</a>
                            </li>
                        </ul>
                    </div>

                    <div class="see-more-home text-left">
                        <a href="">
                            Our Products
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
        
<?php
    include "footer.php";
?>